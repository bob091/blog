from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from taggit.managers import TaggableManager

class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')

class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    CATEGORY =(
        ('film', 'Film'),
        ('books', 'Książki'),
        ('tochnologies', 'Technologie'),
        ('another', 'Inne'),
    )
    title = models.CharField(max_length = 250)
    slug = models.SlugField(max_length = 250, unique_for_date='publish') #krutka etykieta do tworzenia opisów dla SEO
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_posts')
    body = models.TextField()
    publish = models.DateTimeField(default = timezone.now)
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    category = models.CharField(max_length = 12, choices = CATEGORY, default = 'another')
    status = models.CharField(max_length = 10, choices = STATUS_CHOICES, default = 'draft')
    image = models.ImageField(null=True, blank=True) # pole do pobierania obrazu

    class Meta:
        ordering = ('-publish',)
    
    def __str__(self):
        return self.title

    object = models.Manager()
    published = PublishedManager()

    # Metoda wykorzystywana do tworzenia łączy do poszczegulnych postów
    def get_absolute_url(self):
         return reverse('blog:post_detail',
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day, 
                             self.slug
                             ])

    tags = TaggableManager()

class Comment(models.Model):
    post = models.ForeignKey(Post,
                             on_delete=models.CASCADE,
                             related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Metta:
        ordering = ('created',)

    def __str__(self):
        return 'Komentarz dodany przez {} dla posta {}'.format(self.name, self.post)

class AboutAuthor(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField()

    def __str__(self):
        return 'Opis dodany'

class Contact(models.Model):
    title = models.CharField(max_length=80)
    telephone_number = models.CharField(max_length=15)
    email = models.EmailField()

    def __str__(self):
        return 'Dane kontaktowe dodane'