from django.shortcuts import render, get_object_or_404
from .models import Post, AboutAuthor, Contact, Comment
from django.core.paginator import Paginator, EmptyPage,  PageNotAnInteger
from django.views.generic import ListView
from django.core.mail import send_mail
from taggit.models import Tag
from django.db.models import Count

from .forms import EmailPostForm, CommentForm, SearchForm

def post_list(request):
    posts = Post.published.all()
    tags_list = getTag()  # lista tagów do wyświetlenia
    posts_activity_list = getActivity()  # Wyświtalnie listy ostanio dodanych postów
    return render(request, 'blog/post/list.html', {'posts': posts, 'tags_list': tags_list, 'posts_activity_list': posts_activity_list})

def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug = post,
                                   status = 'published',
                                   publish__year = year,
                                   publish__month = month,
                                   publish__day = day)

    tags_list = getTag()  # lista tagów do wyświetlenia
    posts_activity_list = getActivity()  # Wyświtalnie listy ostanio dodanych postów

    #Lista aktywnych komentarzy dla danego posta
    comments = post.comments.filter(active=True)
    if request.method == "POST":
        #komentarz został opublikowany
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            #Utworzenie obiektu comment
            new_comment = comment_form.save(commit=False)
            #Przypisanie komentarza do bierzącego posta
            new_comment.post = post
            #Zapisanie komentarza w bazie danych
            new_comment.save()
    else:
        comment_form = CommentForm()

    """Obsługa proponowania pdobnych postów za pomocą tagó"""
    post_tag_ids = post.tags.values_list('id', flat=True)#flat=True powoduje wyświetleni listyw ppstaci [1,2,3]
    similar_posts = Post.published.filter(tags__in=post_tag_ids).exclude(id = post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags','-publish')[:4]

    return render(request, 'blog/post/detail.html', {'post' : post,
                                                     'comments' : comments,
                                                     'comment_form' : comment_form,
                                                     'similar_posts' : similar_posts,
                                                     'tags_list': tags_list,
                                                     'posts_activity_list' : posts_activity_list})


def post_list(request, tag_slug=None):
    tags_list = getTag()  # lista tagów do wyświetlenia
    posts_activity_list = getActivity()  # Wyświtalnie listy ostanio dodanych postów
    object_list = Post.published.all()
    tag = None


    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags_in=[tag])

    paginator = Paginator(object_list, 3) # Trzy posty na każdej stronie
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        #Jeśli zmienna page nie jest zmienną całkowitą
        # Wtedy pobierana jest pierwsza strona wyników
        posts = paginator.page(1)
    except EmptyPage:
        #Jeśli zmienna page ma nómer więkrzy niż numer ostatniej strony
        #Pobierana zostanie ostatnia strona wyników
        posts = paginator.page(paginator.num_pages)
    return render(request, 'blog/post/list.html',{'page':page, 'posts':posts, 'tag':tag, 'tags_list': tags_list, 'posts_activity_list': posts_activity_list })

#obsułga formularza pozwalającego na wysyłanie postu emailem
def post_share(request, post_id):
    tags_list = getTag()  # lista tagów do wyświetlenia
    posts_activity_list = getActivity()  # Wyświtalnie listy ostanio dodanych postów
    # Pobierz post wg id
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False

    if request.method == 'POST':
        # Formularz przesłano
        form = EmailPostForm(request.POST)
        if form.is_valid():
            # Pola formualarza zostały pomyślnie zwalidowane
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())
            subject = f"{cd['name']} poleca, żebyś przeczytał {post.title}"
            message = f"Read {post.title} at {post_url}\n\n" \
                      f"Komentarzy {cd['name']}: {cd['comments']}"
            send_mail(subject, message, 'admin@myblog.com', [cd['to']])
            sent = True


    else:
        form = EmailPostForm()
    return render(request, 'blog/post/share.html', {'post': post,
                                                    'form': form,
                                                    'sent': sent,
                                                    'tags_list':tags_list,
                                                    'posts_activity_list':posts_activity_list})

def about(request):
    """Obsługa widoku o autorze"""
    about = AboutAuthor.objects.all()
    posts = Post.published.all()
    posts_activity_list = getActivity()  # Wyświtalnie listy ostanio dodanych postów
    tags_list = getTag() #lista tagów do wyświetlenia

    return render(request, 'blog/about.html', {'about': about, 'posts': posts, 'tags_list': tags_list, 'posts_activity_list': posts_activity_list})

def contact(request):
    """Obsługa widoku kontakt"""
    contacts = Contact.objects.all()
    posts = Post.published.all()
    posts_activity_list = getActivity() # Wyświtalnie listy ostanio dodanych postów
    tags_list = getTag()  # lista tagów do wyświetlenia
    return render(request, 'blog/contact.html', {'contacts': contacts, 'posts': posts, 'tags_list':tags_list, 'posts_activity_list': posts_activity_list})

def search(request):
    """Obsługa widoku wyszukiwarki artykłów"""
    form = SearchForm(request.POST)
    posts =None
    if request.method == 'POST':
        if form.is_valid():
            #formularz został wysłany pomyślnie
            object_list = Post.published.all()
            cd = form.cleaned_data
            try:
                post_by_title =  get_object_or_404(Post, title=cd['search'])
                posts = object_list.filter(title=post_by_title)
            except :
              print("Nie znalezione Artykułów")
    else:
        form = SearchForm()
        posts = None


    return render(request, 'blog/search.html ', {'form':form, 'posts':posts })

class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'

"""Metoda przygotowój ąca listę tagów do wyświtlenia """
def getTag():
    tags = Post.tags.all()
    return tags

"""Pobranie trzech ostatnich aktywności(oastanich trzech doadnych postów"""
def getActivity():
   posts_list =Post.published.all()
   posts_list = posts_list.filter().order_by('-publish')[0:3]
   return posts_list

